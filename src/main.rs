use std::thread;

fn main() {
	let cpus = num_cpus::get();
	let mut children = vec![];
	for thread in 0..cpus {
		children.push(thread::spawn(move || {
			spool(thread);
		}))
	}

	for child in children {
		// Wait for the thread to finish. Never returns since they're just loops
		let _ = child.join();
	}
}
fn spool(thread: usize) -> () {
	let mut counter: u128 = 0;
	loop {
		counter += 1;
		if counter % 1000000000 == 0 {
			println!("thread {} has reached {}", thread, counter);
		}
	}
}
